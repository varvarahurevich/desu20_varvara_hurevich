CREATE DATABASE IF NOT EXISTS recruitment_agency;

CREATE SCHEMA IF NOT EXISTS agency_info;

CREATE TABLE IF NOT EXISTS agency_info.address (
	address_id		bigserial PRIMARY KEY,  -- use bigserial to autoincrement primary key
	country			TEXT NOT NULL, 
	city			TEXT NOT NULL,
	district		TEXT NOT NULL,
	street			TEXT NOT NULL,
	house_number	int NOT NULL
);

CREATE TABLE IF NOT EXISTS agency_info.contact_info (
	contact_info_id		bigserial PRIMARY KEY,
	email				TEXT NOT NULL, 
	phone				TEXT NOT NULL, 
	
	CONSTRAINT valid_email CHECK (email ~* '^[A-Za-z0-9]+@[A-Za-z0-9]+[.][A-Za-z]+$'),
	CONSTRAINT valid_phone CHECK (length(phone) = 9 AND phone ~* '[0-9]')
);

CREATE TYPE user_role AS ENUM ('employee', 'interviewer');
CREATE TABLE IF NOT EXISTS agency_info.person (
	person_id			bigserial PRIMARY KEY,
	person_name			TEXT NOT NULL, 
	person_surname		TEXT NOT NULL,
	full_name 			TEXT GENERATED ALWAYS AS (person_name || ' ' || person_surname) STORED,
	birth_date			date, 
	is_blocked			boolean NOT NULL DEFAULT FALSE,
	contact_info_id		bigint REFERENCES agency_info.contact_info (contact_info_id),
	address_id			bigint REFERENCES agency_info.address (address_id), -- may be null
	user_current_role 	user_role DEFAULT 'employee' NOT NULL
	
	-- both employees and employers must be over 18 years old
	CONSTRAINT is_adult CHECK (	(((DATE_PART('year', now()::date) - DATE_PART('year', birth_date)) * 12 +
              					(DATE_PART('month', now()::date) - DATE_PART('month', birth_date))) / 12) >= 18)
);

CREATE TABLE IF NOT EXISTS agency_info.cvs (
	cvs_id				bigserial PRIMARY KEY,
	specialization		TEXT NOT NULL,
	working_experience	int DEFAULT 0, 
	persons_comment		TEXT, 
	person_id			bigint REFERENCES agency_info.person (person_id), 
	
	CONSTRAINT positive_working_experience CHECK (working_experience >= 0)
);

CREATE TABLE IF NOT EXISTS agency_info.companies (
	companies_id			bigserial PRIMARY KEY,
	company_name			TEXT NOT NULL, 
	address_id				bigint REFERENCES agency_info.address (address_id)
);

CREATE TABLE IF NOT EXISTS agency_info.vacancies (
	vacancies_id			bigserial PRIMARY KEY,
	specialization			TEXT NOT NULL, 
	amount_of_places		int NOT NULL, 
	companies_id			bigint REFERENCES agency_info.companies (companies_id),
	
	CONSTRAINT positive_amount_of_places CHECK (amount_of_places >= 0)
);
		
-- that table shows us people who successfully passed the interview and were enrolled in the vacancy
CREATE TABLE IF NOT EXISTS agency_info.employees_to_vacancies (
	employees_to_vacancies_id	bigserial PRIMARY KEY,
	person_id					bigint REFERENCES agency_info.person (person_id) NOT NULL, 
	vacancies_id				bigint REFERENCES agency_info.vacancies (vacancies_id) NOT NULL 
);

CREATE TABLE IF NOT EXISTS agency_info.interviews (
	interviews_id		bigserial PRIMARY KEY,
	person_id			bigint REFERENCES agency_info.person (person_id) NOT NULL,			-- person who would be interviewed
	interview_date		timestamp NOT NULL, 
	vacancies_id		bigint REFERENCES agency_info.vacancies (vacancies_id) NOT NULL		-- vacancy for which the candidate is being interviewed
);	

-- there may be several interviewers to one interview that is why there is table which establish many to many relation
CREATE TABLE IF NOT EXISTS agency_info.interviewers_to_interwiews (
	interviewers_to_interwiews_id		bigserial PRIMARY KEY,
	person_id							bigint REFERENCES agency_info.person (person_id) NOT NULL,
	interviews_id						bigint REFERENCES agency_info.interviews (interviews_id) NOT NULL
);




/* TASK. 
 * Fill your tables with sample data */

INSERT INTO agency_info.address (country, city, district, street, house_number)
VALUES	('belarus', 'minsk', 'frundzensky', 'berestyanskaya', 4),
		('belarus', 'minsk', 'frundzensky', 'berestyanskaya', 5), 
		('belarus', 'minsk', 'partisansky', 'uruchye', 152)
RETURNING *;


INSERT INTO agency_info.contact_info (email, phone) 
VALUES	('test1@gmail.com', '447779877'),
		('test2@gmail.com', '299555465')
RETURNING *;


INSERT INTO agency_info.person (person_name, person_surname, birth_date, contact_info_id, address_id)
VALUES	('varvara', 'hurevich', '2000-08-29'::date, 2, 1),
		('maya', 'hurevich', '1999-04-30'::date, 3, 1), 
		('artyom', 'vorobey', '1999-11-06'::date, 3, 3)
RETURNING *;

INSERT INTO agency_info.person (person_name, person_surname, birth_date, contact_info_id, address_id, user_current_role)
VALUES	('polina', 'yakovets', '1980-08-20'::date, 2, 2, 'interviewer')
RETURNING *;


INSERT INTO agency_info.cvs (specialization, working_experience, persons_comment, person_id)
VALUES	('Python', 3, 'I am hardworking and sociable', 1), 
		('C#', 1, 'I am not afraid of difficult tasks and I am always happy to learn new things', 3)
RETURNING *;


INSERT INTO agency_info.companies (company_name, address_id) 
VALUES	('EPAM', 1), 
		('IsSoft', 2)
RETURNING *;


INSERT INTO agency_info.vacancies (specialization, amount_of_places, companies_id)
VALUES	('C#', 5, 1), 	
		('Python', 4, 2)
RETURNING *;


INSERT INTO agency_info.employees_to_vacancies (person_id, vacancies_id)
VALUES	(1, 2), 
		(3, 1)
RETURNING *;


INSERT INTO agency_info.interviews (person_id, interview_date, vacancies_id)
VALUES	(1, '2020-9-10'::date, 1),
		(2, '2020-8-10'::date, 2),
		(3, '2020-8-10'::date, 2)
RETURNING *;


INSERT INTO agency_info.interviewers_to_interwiews (person_id, interviews_id)
VALUES 	(4, 1),
		(4, 2),
		(4, 3)
RETURNING *;


/* TASK.
 * Alter all tables and add 'record_ts' field to each table.
 *  Make it not null and set its default value to current_date */

ALTER TABLE agency_info.address ADD COLUMN record_ts date DEFAULT now() NOT NULL; 
ALTER TABLE agency_info.companies ADD COLUMN record_ts date DEFAULT now() NOT NULL;
ALTER TABLE agency_info.contact_info ADD COLUMN record_ts date DEFAULT now() NOT NULL;
ALTER TABLE agency_info.cvs ADD COLUMN record_ts date DEFAULT now() NOT NULL;
ALTER TABLE agency_info.employees_to_vacancies ADD COLUMN record_ts date DEFAULT now() NOT NULL;
ALTER TABLE agency_info.interviewers_to_interwiews ADD COLUMN record_ts date DEFAULT now() NOT NULL;
ALTER TABLE agency_info.interviews ADD COLUMN record_ts date DEFAULT now() NOT NULL;
ALTER TABLE agency_info.person ADD COLUMN record_ts date DEFAULT now() NOT NULL;
ALTER TABLE agency_info.vacancies ADD COLUMN record_ts date DEFAULT now() NOT NULL;













