CREATE DATABASE IF NOT EXISTS institution_workload;

CREATE SCHEMA IF NOT EXISTS institution_info;


-- Task 1


CREATE TABLE IF NOT EXISTS institution_info.address (
	address_id 		serial PRIMARY KEY,
  	street		 	TEXT NOT NULL,
 	house_number	INT NOT NULL
);


CREATE TABLE IF NOT EXISTS institution_info.institution (
	institution_id 		serial PRIMARY KEY,
	institution_name	TEXT NOT NULL UNIQUE, 
	address_id			int REFERENCES institution_info.address(address_id)
);

CREATE TABLE IF NOT EXISTS institution_info.personal_info (
	personal_info_id 		serial PRIMARY KEY,
  	first_name			TEXT NOT NULL,
  	last_name		 	TEXT NOT NULL,
  	full_name	 		TEXT GENERATED ALWAYS AS (first_name || ' ' || last_name) STORED,
  	birth_date			date NOT NULL, 
  	phone	 			TEXT NOT NULL,
  	email	 			TEXT,
  	
	CONSTRAINT valid_email CHECK (email ~* '^[A-Za-z0-9]+@[A-Za-z0-9]+[.][A-Za-z]+$'),
	CONSTRAINT valid_phone CHECK (length(phone) = 9 AND phone ~* '[0-9]')
);


CREATE TABLE IF NOT EXISTS institution_info.patient (
	patient_id 			serial PRIMARY KEY,
  	personal_discount	float DEFAULT 0,
  	personal_info_id	INT REFERENCES institution_info.personal_info(personal_info_id)
  	
  	CONSTRAINT valid_personal_discount CHECK (personal_discount <= 1 AND personal_discount >= 0)
);


CREATE TYPE user_role AS ENUM ('doctor', 'cleaner', 'cloakroom attendant', 'accountant');
CREATE TABLE IF NOT EXISTS institution_info.staff (
	staff_id 			serial PRIMARY KEY,
  	staff_role 			user_role NOT NULL DEFAULT 'doctor',
  	active 				boolean NOT NULL DEFAULT TRUE,
  	personal_info_id	INT REFERENCES institution_info.personal_info(personal_info_id)
);


CREATE TABLE IF NOT EXISTS institution_info.capabilitie (
	capabilitie_id 		serial PRIMARY KEY,
	capabilitie_name	TEXT NOT NULL,
  	price	 			INT NOT NULL DEFAULT 0,
  	institution_id 		INT REFERENCES institution_info.institution(institution_id),
  	staff_id	 		INT REFERENCES institution_info.staff(staff_id)
);


-- this is my capacity
CREATE TABLE IF NOT EXISTS institution_info.free_places_for_capabilities (
	free_places_for_capabilities_id serial PRIMARY KEY,
	capabilitie_id					int REFERENCES institution_info.capabilitie(capabilitie_id), 
	amount_of_free_places			int NOT NULL DEFAULT 0
	
	CONSTRAINT valid_amount_of_free_places CHECK (amount_of_free_places >= 0)
);
	
   
CREATE TABLE IF NOT EXISTS institution_info.patient_to_capability (
	patient_to_capability_id	serial PRIMARY KEY, 
	patient_id					int REFERENCES institution_info.patient(patient_id),
	capabilitie_id				int REFERENCES institution_info.capabilitie(capabilitie_id), 
	visit_date					timestamp NOT NULL
);
	

INSERT INTO institution_info.address (street, house_number)
VALUES 		('berestyanskaya', 1),
			('berestyanskaya', 2),
			('berestyanskaya', 3),
			('uruchye', 1),
			('uruchye', 2)
RETURNING *;


INSERT INTO institution_info.institution (institution_name, address_id)
VALUES 		('hospital  №1', 1),
			('hospital  №2', 2),
			('hospital  №3', 3),
			('polyclinic  №1', 4),
			('polyclinic  №2', 5)
RETURNING *;


INSERT INTO institution_info.personal_info (first_name, last_name, birth_date, phone, email)
VALUES 		('varvara', 'hurevich', '2000-08-29'::date, 447779877, 'gurevichvarvara@gmail.com'),
			('maya', 'hurevich', '1999-04-30'::date, 447779877, 'gurevichvarvara@gmail.com'), 
			('artyom', 'vorobey', '1999-11-06'::date, 447779877, 'gurevichvarvara@gmail.com'),
			('poly', 'hurevich', '1999-04-30'::date, 447779877, 'gurevichvarvara@gmail.com'), 
			('ivan', 'vorobey', '1999-11-06'::date, 447779877, 'gurevichvarvara@gmail.com'),
			('varvara', 'volcova', '2000-08-29'::date, 447779877, 'gurevichvarvara@gmail.com'),
			('maya', 'volcova', '1999-04-30'::date, 447779877, 'gurevichvarvara@gmail.com'), 
			('artyom', 'volcov', '1999-11-06'::date, 447779877, 'gurevichvarvara@gmail.com'),
			('poly', 'volcova', '1999-04-30'::date, 447779877, 'gurevichvarvara@gmail.com'), 
			('ivan', 'volcov', '1999-11-06'::date, 447779877, 'gurevichvarvara@gmail.com')
RETURNING *;


INSERT INTO institution_info.patient (personal_info_id, personal_discount)
VALUES 		(1, 0),
			(2, 0.8),
			(3, 0.3),
			(4, 0.1),
			(5, 0)
RETURNING *;


INSERT INTO institution_info.staff (personal_info_id, staff_role)
VALUES 		(6, 'doctor'), 
			(7, 'doctor'),
			(8, 'doctor'),
			(9, 'cleaner'),
			(10, 'accountant')
RETURNING *;


INSERT INTO institution_info.capabilitie (capabilitie_name, institution_id, staff_id)
VALUES 		('surgery', 1, 1),
			('surgery', 1, 2),
			('surgery', 1, 3),
			('therapist', 4, 2),
			('therapist', 4, 3)
RETURNING *;


INSERT INTO institution_info.free_places_for_capabilities(capabilitie_id, amount_of_free_places)
VALUES 		(1, 10),
			(2, 10),
			(3, 10),
			(4, 10),
			(5, 10)
RETURNING *;


INSERT INTO institution_info.patient_to_capability (patient_id, capabilitie_id, visit_date)
VALUES 		(1, 1, '2020-9-22 19:00:00'),
			(2, 1, '2020-9-23 19:00:00'),
			(3, 1, '2020-9-24 19:00:00'),
			(4, 1, '2020-9-25 19:00:00'),
			(5, 1, '2020-9-26 19:00:00'),
			(1, 1, '2020-8-22 19:00:00'),
			(2, 1, '2020-8-23 19:00:00'),
			(3, 1, '2020-8-24 19:00:00'),
			(4, 1, '2020-8-25 19:00:00'),
			(5, 1, '2020-8-26 19:00:00'),
			(5, 2, '2020-8-26 19:00:00'),
			(5, 3, '2020-8-26 19:00:00')
RETURNING *;


-- Task 2

SELECT staff_id, visits_of_september, visits_of_october
FROM 	(SELECT s.staff_id, 	count(CASE WHEN date_part('month', ptc.visit_date) = 9 THEN 1 ELSE NULL END) AS visits_of_september, 
								count(CASE WHEN date_part('month', ptc.visit_date) = 8 THEN 1 ELSE NULL END) AS visits_of_october
		FROM institution_info.patient_to_capability ptc 
		JOIN institution_info.capabilitie c ON c.capabilitie_id  = ptc.capabilitie_id 
		JOIN institution_info.staff s ON s.staff_id = c.staff_id 
		GROUP BY s.staff_id) AS amount_of_visits_of_last_two_month
WHERE visits_of_september < 5 AND visits_of_october < 5;



















