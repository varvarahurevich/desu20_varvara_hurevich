-- inserting Russian language  
INSERT INTO public."language" ("name")
VALUES ('Russian');

CREATE OR REPLACE FUNCTION insert_film_data(IN film_title TEXT, IN film_language TEXT DEFAULT 'Russian', IN film_release_year YEAR DEFAULT DATE_PART('year', now()::date))
RETURNS bigint 
LANGUAGE plpgsql

AS $$

DECLARE inserted_film_id bigint;

BEGIN 
	INSERT INTO public.film (title, release_year, language_id)
	SELECT film_title, film_release_year, lng.language_id
	FROM public."language" lng
	WHERE lng."name" = film_language
	RETURNING film_id INTO inserted_film_id;

	RAISE NOTICE 'Film % is inserted. Its id = %', film_title, inserted_film_id;

	RETURN inserted_film_id;
END;

$$;
