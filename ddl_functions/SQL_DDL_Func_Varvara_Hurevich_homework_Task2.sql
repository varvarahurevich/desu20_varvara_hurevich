/* 	
 * 	TASK. What operations do the following functions perform: film_in_stock, film_not_in_stock, inventory_in_stock, get_customer_balance,
 *	inventory_held_by_customer, rewards_report, last_day? 
 */


-- film_in_stock: 				checking if there are (in specified store) inventory items with current film which are not in rent now or there were in rent but not now
-- film_not_in_stock:			checking if there are (in specified store) inventory items with current film which are in rent now or there were in rent 
-- inventory_in_stock:			checking if the specified inventory item is found in rental and what is its return date equal to
-- get_customer_balance:		computing amount of money user paid before specified date (consists of general rental and two different overdue rentals)
-- inventory_held_by_customer:	checking which customer is holding specified inventory item and didn't return it yet
-- rewards_report: 				showing which customers made more payments and pay more money then were given as an arguments before specified date
-- last_day: 					returns the last day of month of specified date


/*
 *	TASK. 		Correct and recreate the function, so that it's able to return rows properly.
 * 	SOLVING. 	Change last_month_start to maximum payment date 
 */

CREATE OR REPLACE FUNCTION public.rewards_report(min_monthly_purchases integer, min_dollar_amount_purchased numeric)
 RETURNS SETOF customer
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
    last_month_start DATE;
    last_month_end DATE;
rr RECORD;
tmpSQL TEXT;
BEGIN

    /* Some sanity checks... */
    IF min_monthly_purchases = 0 THEN
        RAISE EXCEPTION 'Minimum monthly purchases parameter must be > 0';
    END IF;
    IF min_dollar_amount_purchased = 0.00 THEN
        RAISE EXCEPTION 'Minimum monthly dollar amount purchased parameter must be > $0.00';
    END IF;

   	
    SELECT max(payment_date) INTO last_month_start
    FROM public.payment p;
    last_month_start := to_date((extract(YEAR FROM last_month_start) || '-' || extract(MONTH FROM last_month_start) || '-01'),'YYYY-MM-DD');
    last_month_end := LAST_DAY(last_month_start);

    /*
    Create a temporary storage area for Customer IDs.
    */
    CREATE TEMPORARY TABLE tmpCustomer (customer_id INTEGER NOT NULL PRIMARY KEY);

    /*
    Find all customers meeting the monthly purchase requirements
    */

    tmpSQL := 'INSERT INTO tmpCustomer (customer_id)
        SELECT p.customer_id
        FROM payment AS p
        WHERE DATE(p.payment_date) BETWEEN '||quote_literal(last_month_start) ||' AND '|| quote_literal(last_month_end) || '
        GROUP BY customer_id
        HAVING SUM(p.amount) > '|| min_dollar_amount_purchased || '
        AND COUNT(customer_id) > ' ||min_monthly_purchases ;

    EXECUTE tmpSQL;

    /*
    Output ALL customer information of matching rewardees.
    Customize output as needed.
    */
    FOR rr IN EXECUTE 'SELECT c.* FROM tmpCustomer AS t INNER JOIN customer AS c ON t.customer_id = c.customer_id' LOOP
        RETURN NEXT rr;
    END LOOP;

    /* Clean up */
    tmpSQL := 'DROP TABLE tmpCustomer';
    EXECUTE tmpSQL;

RETURN;
END
$function$
;


/* 
 * 	TASK. 		Is there any function that can potentially be removed from the dvd_rental codebase? If so, which one and why?
 */

--	film_not_in_stock:	isn't necessary, because we already have functions that gives as an opposite result
-- 	las_day:			isn't necessary, because we could easily calculate the last day of the month and not wrap this logic to a single function		



/*
 * 	TASK.		The ‘get_customer_balance’ function describes the business requirements for calculating the client balance. Unfortunately, not all of
 *				them are implemented in this function. Try to change function using the requirements from the comments.
 */


CREATE OR REPLACE FUNCTION public.get_customer_balance(p_customer_id integer, p_effective_date timestamp with time zone)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
       --#OK, WE NEED TO CALCULATE THE CURRENT BALANCE GIVEN A CUSTOMER_ID AND A DATE
       --#THAT WE WANT THE BALANCE TO BE EFFECTIVE FOR. THE BALANCE IS:
       --#   1) RENTAL FEES FOR ALL PREVIOUS RENTALS
       --#   2) ONE DOLLAR FOR EVERY DAY THE PREVIOUS RENTALS ARE OVERDUE
       --#   3) IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE, CHARGE THE REPLACEMENT_COST
       --#   4) SUBTRACT ALL PAYMENTS MADE BEFORE THE DATE SPECIFIED
DECLARE
    v_rentfees DECIMAL(5,2); --#FEES PAID TO RENT THE VIDEOS INITIALLY
    v_overfees INTEGER;      --#LATE FEES FOR PRIOR RENTALS
    v_doubly_overdue integer;--#IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE
    v_payments DECIMAL(5,2); --#SUM OF PAYMENTS MADE PREVIOUSLY
BEGIN
    SELECT COALESCE(SUM(film.rental_rate),0) INTO v_rentfees
    FROM film, inventory, rental
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

    SELECT COALESCE(SUM(CASE 
                           WHEN (rental.return_date - rental.rental_date) > (film.rental_duration * '1 day'::interval)
                           THEN EXTRACT(epoch FROM ((rental.return_date - rental.rental_date) - (film.rental_duration * '1 day'::interval)))::INTEGER / 86400 -- * 1 dollar
                           ELSE 0
                        END),0) 
    INTO v_overfees
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;

     
    SELECT COALESCE(SUM(CASE 
    					   -- count only doubled overdue
                           WHEN (rental.return_date - rental.rental_date) > (film.rental_duration * '2 day'::interval) 
                           -- add only one dollar because we already add one dollar to all overdue 
                           THEN EXTRACT(epoch FROM ((rental.return_date - rental.rental_date) - (film.rental_duration * '1 day'::interval)))::INTEGER / 86400 -- * 1 dollar
                           ELSE 0
                        END),0) 
    INTO v_doubly_overdue
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;
     
    SELECT COALESCE(SUM(payment.amount),0) INTO v_payments
    FROM payment
    WHERE payment.payment_date <= p_effective_date
    AND payment.customer_id = p_customer_id;

    RETURN v_rentfees + v_overfees + v_doubly_overdue - v_payments;
END
$function$
;

/* 
 *	TASK. How do ‘group_concat’ and ‘_group_concat’ functions work? (database creation script might help) Where are they used?
 */

-- group_count: takes two parts of text and gives one which are showing separated by commas
-- _group_count: take a column of text lines and display all its lines as one separated by commas



/*
 * 	TASK. What does ‘last_updated’ function do? Where is it used?
 */

-- it calls every time we modified tables rows it used in and it shows time where modifications were done


/*
 * 	TASK. What is tmpSQL variable for in ‘rewards_report’ function? Can this function be recreated without EXECUTE statement and dynamic SQL? Why?
 */

-- Yes, we could do it assignment query to variable, because we need to put into temp SQL necessary data and it's just two different way to perform it



