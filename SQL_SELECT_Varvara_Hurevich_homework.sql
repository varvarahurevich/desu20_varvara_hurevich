-- All comedy movies released between 2000 and 2004, alphabetical

select * 										-- as I wasn't given by any field selection criteria then I chose all fields
from public.film 								-- all nessesary information is stored in one single table - 'film'
where release_year between 2000 and 2004		-- operation 'between' allows us to select only that rows which values in certain fields are in the required interval
order by title;									-- to show movies alphabetically we need to oreder them by title in increasing oreder


-- !! Revenue of every rental store for year 2017, columns: address and address2 – as one column, revenue

select a.address || ' ' || a.address2 AS store_address  , sum(p.amount) as revenue   	-- I used || ' ' || to create one column from two 
from public.store s
join public.address a on a.address_id = s.address_id 									-- to get payment we need to join table 'store' with
join public.inventory inv on s.store_id = inv.store_id 									-- tables 'inventory' then 'rental'
join public.rental r on r.inventory_id = inv.inventory_id
join public.payment p on p.rental_id = r.rental_id 
where date_part('year', p.payment_date) = 2017
group by s.store_id, a.address_id;														


-- Top-3 actors by number of movies they took part in, columns: first_name, last_name, number_of_movies, sorted by number_of_movies in descending order

select a.first_name as first_name, a.last_name as last_name, amount_of_movies.amount as number_of_movies
from public.actor a 
join 	(select fa.actor_id, count(*) as amount					-- we need to put up info about actor and amount of films he took part in 
		from public.film_actor fa 								-- that is why we extract that info from film_actor table
		group by fa.actor_id) as amount_of_movies				-- by counting films and join this with actor table
on amount_of_movies.actor_id = a.actor_id
order by amount_of_movies.amount desc							-- last step is to sort info by amount of movies in decreasing order
limit 3;														-- and I took top 3


-- Number of comedy, horror and action movies per year (columns: release_year, number_of_action_movies, number_of_horror_movies, number_of_comedy_movies), sorted by release year in descending order

select f.release_year as release_year, 
		count(case when c."name" = 'Action' then 1 else null end) as number_of_action_movies, 	-- as we need to count some generes separately so the best way to do i 
		count(case when c."name" = 'Horror' then 1 else null end) as number_of_horror_movies,	-- is to add to first select some case-statements 
		count(case when c."name" = 'Comedy' then 1 else null end) as number_of_comedy_movies
from public.film f 
join public.film_category fc on f.film_id = fc.film_id 											-- we need information from 'category' table thus I joined 'film' table with 'film-category'										
join public.category c on fc.category_id = c.category_id										-- and then 'film-category' with 'category'
group by f.release_year
order by f.release_year desc;


-- Which staff members made the highest revenue for each store and deserve a bonus for 2017 year?
-- The query'll return id of store, lastname of staff who made highest revenue and his revenue

with total_revenue_for_stores_by_staff_2017 as (										-- table expression contains the amount of revenue each staff made 
	select p.staff_id, s.store_id, sum(p.amount) as revenue								-- for each store 
	join public.rental r on r.staff_id = p.staff_id 
	join public.inventory i on i.inventory_id = r.inventory_id 
	join public.store s on s.store_id = i.store_id 
	where date_part('year', p.payment_date) = 2017
	group by s.store_id, p.staff_id)

select distinct max_revenue_of_stores.store_id, st.last_name, max_revenue_of_stores.max_revenue
from 
	(select store_id, max(revenue) as max_revenue										-- find max value of revenue for each store 
	from total_revenue_for_stores_by_staff_2017								
	group by store_id) as max_revenue_of_stores
join total_revenue_for_stores_by_staff_2017 
on total_revenue_for_stores_by_staff_2017.store_id = max_revenue_of_stores.store_id and total_revenue_for_stores_by_staff_2017.revenue = max_revenue
join public.staff st on st.staff_id = total_revenue_for_stores_by_staff_2017.staff_id	-- find last name of staff who made it
order by max_revenue_of_stores.store_id;


-- Which 5 movies were rented more than others and what's expected audience age for those movies?

select f.title, count(*) as amount_of_being_rented, f.rating
from public.rental r 
join public.inventory i on i.inventory_id = r.inventory_id
join public.film f on i.film_id = f.film_id 
group by f.title, f.rating
order by count(*) desc
limit(5);


-- Which actors/actresses didn't act for a longer period of time than others?

select a.first_name || ' ' || a.last_name, max(f.release_year)
from public.actor a 
join public.film_actor fa on fa.actor_id = a.actor_id 
join public.film f on f.film_id = fa.film_id
group by a.first_name, a.last_name, f.release_year 
order by f.release_year
limit(1);

