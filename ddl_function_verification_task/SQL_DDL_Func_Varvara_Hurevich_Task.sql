CREATE OR REPLACE FUNCTION get_customer_metrics(IN client_id bigint, IN left_boundary date, IN right_boundary date)
RETURNS TABLE (metric_name TEXT, metric_value TEXT)
AS $$
	SELECT 'customer''s info' AS metric_name, first_name || ' ' || last_name || ', ' || email AS metric_value
	FROM public.customer
	WHERE customer_id = client_id
	
	UNION ALL 
	
	SELECT 'num. of films rented', count(DISTINCT inventory_id)::text 
	FROM rental r 
	WHERE customer_id = client_id AND return_date >= left_boundary AND right_boundary <= right_boundary
	GROUP BY customer_id
	
	UNION ALL 
	
	SELECT 'rented films'' titles', CASE WHEN length(string_agg(DISTINCT f.title, ',')) > 0 THEN string_agg(DISTINCT f.title, ',') ELSE 'no film were rented' END 
	FROM rental r 
	JOIN inventory i ON i.inventory_id = r.inventory_id 
	JOIN film f ON f.film_id = i.film_id 
	WHERE customer_id = client_id AND return_date >= left_boundary AND right_boundary <= right_boundary
	
	UNION ALL 
	
	SELECT 'num. of payments', count(*)::TEXT
	FROM payment p
	WHERE customer_id = client_id AND payment_date >= left_boundary AND payment_date <= right_boundary
	GROUP BY customer_id 
	
	UNION ALL 
	
	SELECT 'payments'' amount', sum(amount)::text
	FROM payment p
	WHERE customer_id = client_id AND payment_date >= left_boundary AND payment_date <= right_boundary
	GROUP BY customer_id 
	
$$
LANGUAGE SQL;



