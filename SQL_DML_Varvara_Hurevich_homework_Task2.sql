-- Task 1. Create table ‘table_to_delete’ and fill it with the following query

CREATE TABLE table_to_delete as
insert into table_to_delete 
SELECT 'veeeeeeery_long_string' || x AS col
FROM generate_series(1,(10^7)::int) x;



-- Task 2. Lookup how much space this table consumes with the following query

SELECT *, 	pg_size_pretty(total_bytes) AS total, 
			pg_size_pretty(index_bytes) AS INDEX, pg_size_pretty(toast_bytes) AS toast,
			pg_size_pretty(table_bytes) AS TABLE
FROM ( 	SELECT *, total_bytes-index_bytes-COALESCE(toast_bytes,0) AS table_bytes
		from (	SELECT c.oid, nspname as table_schema,
				relname AS TABLE_NAME,
				c.reltuples AS row_estimate, 
				pg_total_relation_size(c.oid) AS total_bytes, 
				pg_indexes_size(c.oid) AS index_bytes, 
				pg_total_relation_size(reltoastrelid) AS toast_bytes
		FROM pg_class c	
		LEFT JOIN pg_namespace n ON n.oid = c.relnamespace 
		WHERE relkind = 'r'
		) a
	) a
WHERE table_name LIKE '%table_to_delete%'



-- Task 3. Delete operation  

-- execution time 10041.410ms, before vacuum 602537984 bytes, after vacuum 401580032 bytes
EXPLAIN ANALYZE
DELETE FROM table_to_delete
WHERE REPLACE(col, 'veeeeeeery_long_string','')::int % 3 = 0;

VACUUM FULL VERBOSE table_to_delete;

-- recreating table_to_delete table (adding data wich was deleted)
insert into table_to_delete
select 'veeeeeeery_long_string' || x
FROM generate_series(1,(10^7)::int) x
where x % 3 = 0;


-- Task 4. Truncate operation 

-- execution time 0ms, before truncate 602537984 bytes, after truncate 8192 bytes
do $$
declare 
truncate_start_time time;
truncate_finish_time time;

begin
truncate_start_time := now();
truncate table_to_delete;
truncate_finish_time := now();
RAISE notice 'start time is: %', truncate_start_time;
RAISE notice 'finish time is: %', truncate_finish_time;

end $$;



-- Task 5. Hand over your investigation's results to your trainer

select 'DELETE' as operation, 10041.410 as duration_ms, 602537984 as space_before_bytes, 401580032 as space_after_bytes
union all 
select 'TRUNCATE' as operation, 0 as duration_ms, 602537984 as space_before_bytes, 8192 as space_after_bytes


SELECT 'Conclusion: if we use the delete statemnt, then the selected lines are not deleted from the database immediately, we need to clean up the space, which firstly requires additional operations and time, and secondly you can simply forget about it, the truncate operator deletes the lines from the database immediately. However, we cannot select specific rows to delete when using a truncate, which is a big disadvantage, because we need this operation much more often than an operation to delete the entire table. Therefore, if we need to delete the entire table, we need to use a truncate, and if the selected rows then delete.'








