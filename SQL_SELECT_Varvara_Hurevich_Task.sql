/* Task 1. Top-3 most selling movie categories of all time  
	and total dvd rental income for each category (I understood that I should show incomes only for top-3 best sellers).
	Only consider dvd rental customers from the USA. */

select 	cat."name" as category, count(pmt.payment_id) as amount_of_sellings, sum(pmt.amount) as total_income
from payment pmt
join public.rental r on r.staff_id = pmt.rental_id 
join public.inventory i on i.inventory_id = r.inventory_id 
join public.film f on i.film_id = f.film_id

join public.film_category fc on f.film_id = fc.film_id 																					
join public.category cat on fc.category_id = cat.category_id

join public.customer cst on cst.customer_id = r.customer_id 
join public.address a on a.address_id = cst.address_id 
join public.city c on c.city_id = a.city_id 
join public.country cntr on cntr.country_id = c.country_id  
where upper(cntr.country) = 'UNITED STATES'

group by cat."name"
order by amount_of_sellings desc
limit 3;


/* 	Task 2. For each client, display a list of horrors that he had ever rented (in one column, separated by commas),
  	and the amount of money that he paid for it */


select cst.first_name || ' ' || cst.last_name as customer_name,  string_agg(f.title, ',') as horror_list, sum(p.amount)
from public.customer cst 
join public.rental r on r.customer_id = cst.customer_id 
join public.payment p on p.rental_id = r.rental_id 
join public.inventory inv on r.inventory_id = inv.inventory_id
join public.film f on inv.film_id = f.film_id
join public.film_category fc on f.film_id = fc.film_id 																					
join public.category cat on fc.category_id = cat.category_id
where upper(cat."name") = 'HORROR'
group by cst.first_name, cst.last_name;
	