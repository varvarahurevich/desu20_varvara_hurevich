/*	TASK. Choose your top-3 favorite movies and add them to 'film' table. Fill rental rates with 4.99, 9.99 and 19.99 
	and rental durations with 1, 2 and 3 weeks respectively */

insert into public.film(title, release_year, language_id, rental_rate, rental_duration)
values 	('1+1', 2011, 1, 4.99, 1),
		('Pretty Woman', 1990, 1, 9.99, 2),
		('The Curious Case of Benjamin Button', 2008, 1, 19.9, 3)

-- use returning to see the result of operation
returning *;


		

-- TASK. Add actors who play leading roles in your favorite movies to 'actor' and 'film_actor' tables (6 or more actors in total)

-- use cte statement for later inserting actor's ids to film_actor table
with one_plus_one_actors_id as 	(insert into public.actor (first_name, last_name)
								values 	('Omar', 'Sy'), 
										('Audrey', 'Fleurot')
								returning actor_id)
					
-- we don't have relation between film and actor table thus adding actors to every film separately 
insert into public.film_actor (actor_id, film_id)
select actor_id, f.film_id
from one_plus_one_actors_id cross join public.film f
where f.title = '1+1'
returning *;

with pretty_woman_actors_id as 	(insert into public.actor (first_name, last_name)
								values 	('Julia', 'Roberts'), 
										('Richard', 'Gere')
								returning actor_id)

insert into public.film_actor (actor_id, film_id)
select actor_id, f.film_id
from pretty_woman_actors_id cross join public.film f
where f.title = 'Pretty Woman'
returning *;

with benjamin_button_actors_id as 	(insert into public.actor (first_name, last_name)
									values 	('Brad', 'Pitt'), 
											('Cate', 'Blanchett')
									returning actor_id)

insert into public.film_actor (actor_id, film_id)
select actor_id, f.film_id
from benjamin_button_actors_id cross join public.film f
where f.title = 'The Curious Case of Benjamin Button'
returning *;



-- TASK. Add your favorite movies to any store's inventory

insert into public.inventory (film_id, store_id) 
select f.film_id, st.store_id 
from public.store st cross join public.film f
where f.title = '1+1' or f.title = 'Pretty Woman' or f.title = 'The Curious Case of Benjamin Button'
returning *;



/* 	TASK. Alter any existing customer in the database who has at least 43 rental and 43 payment records. 
 	Change his/her personal data to yours (first name, last name, address, etc.). Do not perform any updates on 'address' 
	table, as it can impact multiple records with the same address. Change
	customer's create_date value to current_date */

-- as I can't update any rows in address table that's why I'm adding my personal address information
insert into public.city(city, country_id)
select 'Minsk', country_id 
from public.country cnt
where country = 'Belarus';

with my_address as (insert into public.address (address, district, city_id, phone)
					select '4 Berestyanskaya', 'Partisan district', city_id, '447779877'
					from public.city c 
					where city = 'Minsk'
					returning address_id)

update public.customer  
set first_name = 'Varvara', last_name = 'Hurevich', email = 'gurevichvarvara@gmail.com', create_date = current_date, 
	address_id = 	(select * from my_address)
where customer_id in 	(select customer_id
						from public.payment pmnt
						where customer_id in	(select customer_id 
												from public.rental rnt
												group by customer_id
												having count(*) >= 43)
						group by customer_id 
						having count(*) >= 43);
					

					
-- TASK. Remove any records related to you (as a customer) from all tables except 'Customer' and 'Inventory'

-- tables we should remove records from are payment and rental  

delete from public.payment pmnt
using public.customer cst 
where cst.customer_id = pmnt.customer_id and cst.first_name = 'Varvara' and cst.last_name = 'Hurevich';

-- there are some payments where person who pay for rent is not a person who commit that rent, that rows I should delete too
delete from public.payment pmnt
using public.rental r
where r.rental_id = pmnt.rental_id and r.customer_id in 	(select customer_id
															from public.customer cst
															where cst.first_name = 'Varvara' and cst.last_name = 'Hurevich');			

delete from public.rental r
where r.customer_id in 	(select customer_id
						from public.customer cst
						where cst.first_name = 'Varvara' and cst.last_name = 'Hurevich');

					
	
					
-- TASK. Rent you favorite movies from the store they are in and pay for them (add corresponding records to the database to represent this activity)

-- insert data into rental table 
insert into public.rental (rental_date, inventory_id, customer_id, return_date, staff_id)					
select '2007-02-18 04:54:29', inv.inventory_id, cst.customer_id, '2007-02-20 04:54:29', 1
from public.inventory inv
join public.film f on f.film_id = inv.film_id and (f.title = '1+1' or f.title = 'Pretty Woman' or f.title = 'The Curious Case of Benjamin Button')
cross join public.customer cst
where cst.customer_id =		(select customer_id
							from public.customer cst
							where cst.first_name = 'Varvara' and cst.last_name = 'Hurevich'
							order by cst.customer_id 
							limit(1))
returning *;

-- inset data into payment table
insert into public.payment (customer_id, staff_id, rental_id, amount, payment_date)
select r.customer_id, r.staff_id, r.rental_id, f.rental_rate, '2017-02-21 12:35:45.996577' 
from public.rental r 
join public.inventory inv on inv.inventory_id = r.inventory_id 
join public.film f on f.film_id = inv.film_id 
where r.customer_id =	(select customer_id
						from public.customer cst
						where cst.first_name = 'Varvara' and cst.last_name = 'Hurevich'
						order by cst.customer_id 
						limit(1))
returning *;





















